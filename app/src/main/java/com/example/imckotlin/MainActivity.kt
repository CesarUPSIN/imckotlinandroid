package com.example.imckotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button
    private lateinit var txtAltura: EditText
    private lateinit var txtPeso: EditText
    private lateinit var txtIMC: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)
        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        txtIMC = findViewById(R.id.txtIMC)

        btnCalcular.setOnClickListener {
            if (txtAltura.text.toString().isEmpty() || txtPeso.text.toString().isEmpty()) {
                Toast.makeText(this@MainActivity, "Falta ingresar datos", Toast.LENGTH_SHORT).show()
            } else {
                calcularIMC()
            }
        }

        btnLimpiar.setOnClickListener {
            txtAltura.text.clear()
            txtPeso.text.clear()
            txtIMC.text.clear()
            txtAltura.requestFocus()
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }

    private fun calcularIMC() {
        val peso = txtPeso.text.toString().toFloat()
        val altura = txtAltura.text.toString().toFloat()

        val imc = peso / (altura * altura)

        val imcResultado = String.format("%.2f", imc)
        txtIMC.setText(imcResultado)
    }
}